#pragma once

#include <stdint.h>

#define OW_RESET_FAILED -32700
#define OW_BAD_CRC      -32701

#define OW_SEARCH_FOUND 0
#define OW_SEARCH_DONE  1
#define OW_SEARCH_FAIL  2
#define OW_SEARCH_RESET 3

typedef struct OWSearchState {

    // The highest bit position where a bit was ambiguous and a zero was written
    int8_t lastZeroBranch;

    // Internal flag to indicate if the search is complete
    // This flag is set once there are no more branches to search
    uint8_t done;
    
    uint8_t knownIndex;

    // Discovered 64-bit device address (LSB first)
    // After a successful search, this contains the found device address.
    // During a search this is overwritten LSB-first with a new address.
    uint8_t address[8];

} OWSearchState;

void oneWireInit(void);

void oneWireSearchStart(OWSearchState *state);
uint8_t oneWireSearchNext(OWSearchState *state);

int16_t oneWireTemperature(uint8_t index);
