#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>

#include "Descriptors.h"
#include "board.h"

#include <LUFA/Drivers/Board/LEDs.h>
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Platform/Platform.h>

#include "onewire.h"
#include "pumps.h"
#include "adc.h"
#include "wifi.h"


/* Macros: */
/** LED mask for the library LED driver, to indicate that the USB interface is not ready. */
#define LEDMASK_USB_NOTREADY      LEDS_LED1

/** LED mask for the library LED driver, to indicate that the USB interface is enumerating. */
#define LEDMASK_USB_ENUMERATING  (LEDS_LED2 | LEDS_LED3)

/** LED mask for the library LED driver, to indicate that the USB interface is ready. */
#define LEDMASK_USB_READY        (LEDS_LED2 | LEDS_LED4)

/** LED mask for the library LED driver, to indicate that an error has occurred in the USB interface. */
#define LEDMASK_USB_ERROR        (LEDS_LED1 | LEDS_LED3)

USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface =
  {
   .Config =
   {
    .ControlInterfaceNumber   = INTERFACE_ID_CDC_CCI,
    .DataINEndpoint           =
    {
     .Address          = CDC_TX_EPADDR,
     .Size             = CDC_TXRX_EPSIZE,
     .Banks            = 1,
    },
    .DataOUTEndpoint =
    {
     .Address          = CDC_RX_EPADDR,
     .Size             = CDC_TXRX_EPSIZE,
     .Banks            = 1,
    },
    .NotificationEndpoint =
    {
     .Address          = CDC_NOTIFICATION_EPADDR,
     .Size             = CDC_NOTIFICATION_EPSIZE,
     .Banks            = 1,
    },
   },
  };



/** Standard file stream for the CDC interface when set up, so that the virtual CDC COM port can be
 *  used like any regular character stream in the C APIs.
 */
static FILE USBSerialStream;



void readout(void) {
    
  float temp = oneWireTemperature(0)/16.0;
  fprintf(&USBSerialStream, "Temp: %.2f\t", temp);    
  
  for (uint8_t i=0;i<2;i++) {
    LTC2420* adc = adcReadAvg(i);
    fprintf(&USBSerialStream, "ADC%d: mv=%.2f\t", i, adc->mv);
    
    if (i==0) {
      float mvPerpH = 0.198*(temp+273.15);
      float ph = 7-(adc->mv / mvPerpH);
      
      fprintf(&USBSerialStream, "pH %.2f (%f mV/pH)\t", ph, mvPerpH);
    }
    
  }
    
  fprintf(&USBSerialStream, "\r\n");      
  
  
/*  LTC2420* adc0 = adcReadAvg(0);
  fprintf(&USBSerialStream, "ADC: raw=%lu\tavg=%lu\tmv=%lu\r\n",
          adc0->raw, adc0->avg, adc0->mv);
          */
//  fprintf(&USBSerialStream, "Temp: %.2f\tADC0: %lu\tADC1: %lu\r\n",
//          oneWireTemperature()/16.0, adc0->avg, adcRead(1));
}

void printBytes(uint8_t *bytes, uint8_t count) {
  fprintf(&USBSerialStream, "  {");
  while (count--) {
    fprintf(&USBSerialStream, "0x%02x%c",
            *(bytes++),
            count ? ',' : '}'
    );    
  }
  fprintf(&USBSerialStream, ",");  
}

void search(void) {
  OWSearchState state;
  oneWireSearchStart(&state);
  uint8_t searchError;
  while (!(searchError = oneWireSearchNext(&state))) {
    printBytes(state.address, 8);
    if (state.knownIndex == 0xff) {
      fprintf(&USBSerialStream, " // new!\r\n");
    } else {
      fprintf(&USBSerialStream, " // %d\r\n", state.knownIndex);
    }
  }
  if (searchError != OW_SEARCH_DONE) {
    fprintf(&USBSerialStream, "Search failed %x\r\n", searchError);
  }
}


void handleInput(void) {
  while (1) {
    int16_t ch = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
    if (ch < 0 || ch == ' ' || ch == '\t') {
      // No input, bail out.
      return;
    }

    if (ch == 'W') {
      fprintf(&USBSerialStream, "Lines received: %d\r\n", wifiLineCount());
      uint8_t nl = 0;
      uint8_t ch;
      while ((ch=getRx())) {
        if (nl && ch != '\r') {
          fprintf(&USBSerialStream, "\r");
          nl = 0;
        }
        fprintf(&USBSerialStream, "%c", ch);      
        if (ch == '\n') {
          nl = 1;
        }
      }
      fprintf(&USBSerialStream, "\r\n");
      
            
    } else if (ch == 'w') {
      wifiConfig();
    } else if (ch == 'r') {
      readout();    
      
    } else if (ch == 's') {
      search();     
    } else {
      fprintf(&USBSerialStream, "Ignoring input '%c'\r\n", ch);
    }
    
    //TODO: handleInputChar(ch);
  }
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void initBoard(void) {
  LEDs_Init();
  USB_Init();
  oneWireInit();
  pumpsInit();
  adcInit();
  wifiInit();  
}



/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */
int main(void) {
  initBoard();
  pumpsCirculate(1);

  MCUSR &= ~(1 << WDRF);
  wdt_enable(WDTO_4S);
  clock_prescale_set(clock_div_1);  
  

  /* Create a regular character stream for the interface so that it can be used with the stdio.h functions */
  CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);

  LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
  GlobalInterruptEnable();
  
  pumpsCirculate(1);
  uint16_t chill = 0;
  for (;;) {
    wdt_reset();

    handleInput();

    CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
    USB_USBTask();
    
    if (chill++ > 5000) {
      //readout();    
      chill = 0;
    }
  }
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
  LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
  LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
  bool ConfigSuccess = true;

  ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);

  LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
  CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}

/** CDC class driver callback function the processing of changes to the virtual
 *  control lines sent from the host..
 *
 *  \param[in] CDCInterfaceInfo  Pointer to the CDC class interface configuration structure being referenced
 */
void EVENT_CDC_Device_ControLineStateChanged(USB_ClassInfo_CDC_Device_t *const CDCInterfaceInfo)
{
  /* You can get changes to the virtual CDC lines in this callback; a common
     use-case is to use the Data Terminal Ready (DTR) flag to enable and
     disable CDC communications in your application when set to avoid the
     application blocking while waiting for a host to become ready and read
     in the pending data from the USB endpoints.
  */
  //bool HostReady = (CDCInterfaceInfo->State.ControlLineStates.HostToDevice & CDC_CONTROL_LINE_OUT_DTR) != 0;
}
