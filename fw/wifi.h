#pragma once

#include <stdint.h>

void wifiInit(void);
uint8_t getRx(void);
uint8_t wifiLineCount(void);
void wifiConfig(void);
