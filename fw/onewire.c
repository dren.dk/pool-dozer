#include "board.h"
#include "onewire.h"
#include "delay.h"
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <string.h>

#define KNOWN_ID_COUNT 2
const uint8_t KNOWN_ID[KNOWN_ID_COUNT][8] PROGMEM = {
  {0x28,0xff,0x12,0x0c,0xc1,0x17,0x04,0xbe}, 
  {0x28,0xff,0x8c,0x34,0xc1,0x17,0x05,0xa1},
};

// Lifted from http://www.technoblogy.com/show?1LJD

const uint8_t SEARCH_ROM = 0xf0;
const int READ_ROM = 0x33;
const int MATCH_ROM = 0x55;
const int SKIP_ROM = 0xCC;
const int CONVERT_TEMP = 0x44;
const int READ_SCRATCH_PAD = 0xBE;

void oneWireInit(void) {
  GPINPUT(ONEWIRE);
  GPCLEAR(ONEWIRE);
}

inline void pinLow(void) {
  GPOUTPUT(ONEWIRE);
}

inline void pinRelease(void) {
  GPINPUT(ONEWIRE);
}

inline uint8_t pinRead(void) {
  return GPREAD(ONEWIRE) ? 1 : 0;
}

void lowRelease(int low, int high) {
  pinLow();
  delayUs(low);
  pinRelease();
  delayUs(high);
}

void writeBit(uint8_t data) {
  uint8_t del = data & 1 ? 6 : 60;    
  lowRelease(del, 70 - del);
}

uint8_t readBit(void) {
  lowRelease(2, 10);
  uint8_t bit = pinRead();
  delayUs(50);
  return bit;
}

uint8_t oneWireReset(void) {
  uint8_t data = 1;
  lowRelease(480, 70);
  data = pinRead();
  delayUs(410);
  return data;                         // 0 = device present
}

void oneWireWrite(uint8_t data) {
  for (uint8_t i = 0; i<8; i++) {
    writeBit(data & 1);
    data >>= 1;
  }
}

uint8_t oneWireRead(void) {
  uint8_t data = 0;
  for (uint8_t i = 0; i<8; i++) {
    if (readBit()) {
      data |= 1<<i;
    }
  }
  return data;
}


static union {
  uint8_t bytes[9];
  int16_t words[4];
  struct {
    uint8_t crc;
    uint64_t i64;    
  } longlong;
} oneWireData;

// Read bytes into array, least significant byte first
void oneWireReadBytes(uint8_t bytes) {
  for (int i=0; i<bytes; i++) {
    oneWireData.bytes[i] = oneWireRead();
  }
}

// Calculate CRC over buffer - 0x00 is correct
uint8_t oneWireCRC(uint8_t bytes) {
  uint8_t crc = 0;
  for (uint8_t j=0; j<bytes; j++) {
    crc = crc ^ oneWireData.bytes[j];
    for (int i=0; i<8; i++) crc = crc>>1 ^ ((crc & 1) ? 0x8c : 0);
  }
  return crc;
}
/*
uint64_t oneWireFind(void) {
  for (uint8_t j=0; j<9; j++) {
    oneWireData.bytes[j] = 0;
  }
  
  cli();
  if (oneWireReset() != 0) {
    sei();
    return OW_ADDRESS_RESET_FAILED;
  } else {
    oneWireWrite(READ_ROM);
    oneWireReadBytes(8);
    sei();
    if (oneWireCRC(8) == 0) {
      return oneWireData.longlong.i64;
    } else {
      return OW_ADDRESS_BAD_CRC;
    }
  }
}

int16_t oneWireTemperature() {
  cli();                            // No interrupts
  if (oneWireReset() != 0) {
    sei();
    return OW_RESET_FAILED;
  } else {
    oneWireWrite(SKIP_ROM);
    oneWireWrite(CONVERT_TEMP);
    while (oneWireRead() != 0xFF);
    oneWireReset();
    oneWireWrite(SKIP_ROM);
    oneWireWrite(READ_SCRATCH_PAD);
    oneWireReadBytes(9);
    sei();                          // Interrupts
    if (oneWireCRC(9) == 0) {
      return oneWireData.words[0];
    } else {
      return OW_BAD_CRC;
    }
  }
}

*/

void oneWireMatchRom(uint8_t index) {
    oneWireWrite(MATCH_ROM);
    for (int i=0;i<8;i++) {
      uint8_t byte = pgm_read_byte(&(KNOWN_ID[index][i]));
      oneWireWrite(byte);
    }
}

int16_t oneWireTemperature(uint8_t index) {
  cli();                            // No interrupts
  if (oneWireReset() != 0) {
    sei();
    return OW_RESET_FAILED;
  } else {
    oneWireMatchRom(index);
    oneWireWrite(CONVERT_TEMP);
    while (oneWireRead() != 0xFF);
    oneWireReset();
    oneWireMatchRom(index);
    oneWireWrite(READ_SCRATCH_PAD);
    oneWireReadBytes(9);
    sei();                          // Interrupts
    if (oneWireCRC(9) == 0) {
      return oneWireData.words[0];
    } else {
      return OW_BAD_CRC;
    }
  }
}


void oneWireSearchStart(OWSearchState *state) {
  state->lastZeroBranch = -1;
  state->done = 0;

  // Zero-fill the address
  memset(state->address, 0, sizeof(state->address));
}

uint8_t oneWireSearchNext(OWSearchState *state) {
  // Bail out if the previous search was the end
  if (state->done) {
    return OW_SEARCH_DONE;
  }
  
  cli();
  if (oneWireReset()) {
    // No devices present on the bus
    sei();
    return OW_SEARCH_RESET;
  }
  
  oneWireWrite(SEARCH_ROM);
  
  // States of ROM search reads
  enum {
    kConflict = 0b00,
    kZero = 0b10,
    kOne = 0b01,
  };
  
  // Value to write to the current position
  uint8_t bitValue = 0;
  
  // Keep track of the last zero branch within this search
  // If this value is not updated, the search is complete
  int8_t localLastZeroBranch = -1;
  
  for (uint8_t bitPosition = 0; bitPosition < 64; bitPosition++) {
    
    // Calculate bitPosition as an index in the address array
    uint8_t byteIndex = bitPosition >> 3;
    uint8_t bitIndex  = bitPosition & 7;
    
    // Read the current bit and its complement from the bus
    uint8_t reading = 0;
    reading |= readBit(); // Bit
    reading |= readBit() << 1; // Complement of bit (negated)
    
    switch (reading) {
      case kZero:
      case kOne:
        // Bit was the same on all responding devices: it is a known value
        // The first bit is the value we want to write (rather than its complement)
        bitValue = (reading & 0x1);
        break;
        
      case kConflict:
        // Both 0 and 1 were written to the bus
        // Use the search state to continue walking through devices
        if (bitPosition == state->lastZeroBranch) {
          // Current bit is the last position the previous search chose a zero: send one
          bitValue = 1;
          
        } else if (bitPosition < state->lastZeroBranch) {
          // Before the lastZeroBranch position, repeat the same choices as the previous search
          bitValue = state->address[byteIndex] & (1 << bitIndex);
          
        } else {
          // Current bit is past the lastZeroBranch in the previous search: send zero
          bitValue = 0;
        }
        
        // Remember the last branch where a zero was written for the next search
        if (bitValue == 0) {
          localLastZeroBranch = bitPosition;
        }
        
        break;
        
      default:
        // If we see "11" there was a problem on the bus (no devices pulled it low)
        sei();
        return OW_SEARCH_FAIL;
    }
    
    // Write bit into address
    if (bitValue == 0) {
      state->address[byteIndex] &= ~(1 << bitIndex);
    } else {
      state->address[byteIndex] |= (bitValue << bitIndex);
    }
    
    // Write bit to the bus to continue the search
    writeBit(bitValue);
  }
  
  // If the no branch points were found, mark the search as done.
  // Otherwise, mark the last zero branch we found for the next search
  if (localLastZeroBranch == -1) {
    state->done = 1;
  } else {
    state->lastZeroBranch = localLastZeroBranch;
  }
  
  state->knownIndex = 0xff;
  for (uint8_t i=0;i<KNOWN_ID_COUNT;i++) {
    uint8_t match = 1;
    for (uint8_t b=0;b<8;b++) {
      uint8_t byte = pgm_read_byte(&(KNOWN_ID[i][b]));
      if (state->address[b] != byte) {
        match = 0;
      }
    }
    if (match) {
      state->knownIndex = i;
    }
  }
  
  // Read a whole address - return success
  sei();
  return OW_SEARCH_FOUND;
}


