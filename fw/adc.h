#pragma once

#include <stdint.h>

#define ADC_TIMEOUT 0xf000000
#define ADC_BAD 0xf000001
#define ADC_OUT_OF_RANGE_HIGH 0xf000002
#define ADC_OUT_OF_RANGE_LOW  0xf000003


typedef struct LTC2420 {
  uint32_t raw;
  int32_t avg;  
  float mv;
  int32_t cal_zero;
  int32_t cal_scale;  
} LTC2420;


void adcInit(void);
uint32_t adcRead(uint8_t adc);

LTC2420* adcReadAvg(uint8_t adcIndex);
