#include "adc.h"

#include "board.h"

#define EOC _BV(23)
#define DMY _BV(22)
#define SIG _BV(21)
#define EXR _BV(20)

#define ADC_DELAY 50

struct LTC2420 adcs[] = {
  {0,0,0, 524406, 418.805f},
  {0,0,0, 524685, 419.800f} 
};


void adcInit(void) {
  GPOUTPUT(ADC_SCK);
  GPOUTPUT(ADC_CS1);
  GPOUTPUT(ADC_CS2);
  GPSET(ADC_CS1);
  GPSET(ADC_CS2);
  GPINPUT(ADC_SD);
}

uint8_t adcBit(void) {
  GPSET(ADC_SCK);
  delayUs(ADC_DELAY);
  uint8_t bit = GPREAD(ADC_SD);
  GPCLEAR(ADC_SCK);
  delayUs(ADC_DELAY);
  return bit;
}

uint32_t adcRead(uint8_t adc) {
  GPSET(DEBUG1);
  if (adc) {
    GPCLEAR(ADC_CS1);
    GPSET(ADC_CS2);
  } else {
    GPSET(ADC_CS1);
    GPCLEAR(ADC_CS2);
  }

  delayUs(ADC_DELAY);
  
  // Wait for ~EOC state
  while (GPREAD(ADC_SD)) {   
  }
  
  uint32_t data = 0;
  uint8_t bitsToGo = 24;
  while (bitsToGo--) {
    data <<= 1;
    if (adcBit()) {
      data |= 1;
    }
  }
  
  GPSET(ADC_CS1);
  GPSET(ADC_CS2);
  GPCLEAR(DEBUG1);
  
  if (EOC & data) {
    return ADC_TIMEOUT;
  }
  
  if (DMY & data) {
    return ADC_BAD;
  }
  
  if (EXR & data) {
    if (SIG & data) {
      return ADC_OUT_OF_RANGE_HIGH;
    } else {
      return ADC_OUT_OF_RANGE_LOW;
    }
  } 

  return data & 0xfffff;
}

LTC2420* adcReadAvg(uint8_t adcIndex) {
  uint32_t raw = adcRead(adcIndex);
  LTC2420* adc = adcs+adcIndex;  
  adc->raw = raw;
     
  if (raw >= ADC_TIMEOUT) {
    return adc;
  } 
  
  if (adc->avg == 0) {
    adc->avg = raw;
  } else {
    int32_t delta = raw-adc->avg;    
    adc->avg += delta / 10;
  }

  float mv = adc->avg - adc->cal_zero;
  adc->mv = mv/adc->cal_scale;
  
  return adc;  
}
