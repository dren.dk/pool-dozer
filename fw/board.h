#pragma once

#include "avr8gpio.h"

/*
| Define         | AVR | Pin | Description |
|----------------|-----|-----|-------------|
|                | PD3 | 1   | TX |
|                | PD2 | 2   | RX |
|                | PD1 | 5   | SDA         |
|                | PD0 | 6   | SCL         |
| ONEWIRE        | PD4 | 7   | 1-Wire bus  |
|                | PC6 | 8   |             |
|                | PD7 | 9   |             |
| PUMP0          | PE6 | 10  |  |
| PUMP1          | PB4 | 11  |             |
| PUMP2          | PB5 | 12  |             |
| DEBUG          | PB6 | 13  |             |
|                | PB2 | 14  |             |
|                | PB3 | 15  |             |
|                | PB1 | 16  |             |
| ADC_SD        | PF7 | 17  |  Data input from ADCs |
| ADC_CS1        | PF6 | 18  | Chip select for ADC 1 |
| ADC_CS2        | PF5 | 19  | Chip select for ADC 2 |
| ADC_SCK         | PF4 | 20  | Serial clock for ADCs |

*/

#define TWI_HARDWARE
#define TWI_SDA         GPD1
#define TWI_SCL         GPD0

#define LED_YELLOW     GPB0
#define LED_GREEN      GPD5

// See onewire.c
#define ONEWIRE GPD4

// See pumps.c
#define PUMP0   GPE6
#define PUMP1   GPB4
#define PUMP2   GPB5

// See adc.c
#define ADC_SCK GPF4
#define ADC_CS1 GPF6
#define ADC_CS2 GPF5
#define ADC_SD  GPF7

#define DEBUG1 GPB6

#define UART_BAUD 115200
