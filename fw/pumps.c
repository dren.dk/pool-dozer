#include "pumps.h"

void pumpsInit(void) {
  GPOUTPUT(PUMP0);
  GPOUTPUT(PUMP1);
  GPOUTPUT(PUMP2);  
}

void pumpsCirculate(uint16_t millis) {
  if (millis) {
    GPSET(PUMP0);
  } else {
    GPCLEAR(PUMP0);
  }
}


void pumpsRun1(uint16_t millis) {
  // Turn on pump1 and turn off pump2
  // When the number of mills has passed turn off both pumps
}

void pumpsRun2(uint16_t millis) {
  // When the number of mills has passed turn off both pumps
}
