# Pro-micro pinout

| Define         | AVR | Pin | Description |
|----------------|-----|-----|-------------|
|                | PD3 | 1   | RX |
|                | PD2 | 2   | TX |
|                | PD1 | 5   | SDA         |
|                | PD0 | 6   | SCL         |
| ONEWIRE        | PD4 | 7   | 1-Wire bus  |
|                | PC6 | 8   |             |
|                | PD7 | 9   |             |
|                | PE6 | 10  |  |
| PUMP1          | PB4 | 11  |             |
| PUMP2          | PB5 | 12  |             |
|                | PB6 | 13  |             |
|                | PB2 | 14  |             |
|                | PB3 | 15  |             |
|                | PB1 | 16  |             |
| ADC_SCK        | PF7 | 17  | Serial clock for ADCs |
| ADC_CS1        | PF6 | 18  | Chip select for ADC 1 |
| ADC_CS2        | PF5 | 19  | Chip select for ADC 2 |
| ADC_SD         | PF4 | 20  | Data input from ADCs |


# Chemistry

* Do not test while pump is running, air increases pH temporarily
* First achive ideal pH of 7.2-7.6 (aim for 7.4)
* When pH is in the ideal range, try to hit 1-3 ppm free clorine (aim for 2 ppm)

* Alkalinity 80-120 ppm


# State machine

* Measure temperature
* Measure PH
* Measure ORP
* Measure jet pump state
* Report measurements

* If more than 30 minutes since the last time a pump (including jet) was run, regulate:
 * If PH > 7.2 run pump 1
 * If PH > 7.0 && PH < 7.2 and orp < 1 run pump 2

Note: power-cycle counts as a pump running, for safety.