#include "wifi.h"
#include "board.h"
#include "delay.h"

#include <stdint.h>
#include <stdio.h>

#include <avr/pgmspace.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "LUFA/Drivers/Misc/RingBuffer.h"

#define AT(str) printf_P(PSTR("AT%s\r\n"), str)
#define P(format, ...) printf_P(PSTR(format), __VA_ARGS__)
#define L(str) puts_P(PSTR(str))

#if defined(UCSR0A)

#define UCSRnA UCSR0A
#define UCSRnB UCSR0B
#define UDREn UDRE0
#define UDRn UDR0
#define FEn FE0
#define RXCn RXC0
#define DORn DOR0
#define RXENn RXEN0
#define TXENn TXEN0
#define RXCIEn RXCIE0
#define TXCIEn TXCIE0
#define U2Xn U2X0
#define UBRRnH UBRR0H
#define UBRRnL UBRR0L


#elif  defined(UCSR1A)

#define UCSRnA UCSR1A
#define UCSRnB UCSR1B
#define UDREn UDRE1
#define UDRn UDR1
#define FEn FE1
#define RXCn RXC1
#define DORn DOR1
#define RXENn RXEN1
#define TXENn TXEN1
#define RXCIEn RXCIE1
#define TXCIEn TXCIE1
#define U2Xn U2X1
#define UBRRnH UBRR1H
#define UBRRnL UBRR1L

#else
#error "Neither uart 0 or uart 1 exist"
#endif

enum WifiState {
  WS_INIT=0,
  WS_CONFIG=1,
  WS_READY=2
};

RingBuffer_t txBuffer;
uint8_t      txData[128];

RingBuffer_t rxBuffer;
uint8_t      rxData[256];

RingBuffer_t mixBuffer;
uint8_t      mixData[128];

uint8_t lineCount=0;

uint8_t getRx(void) {
  if (RingBuffer_IsEmpty(&rxBuffer)) {
    return 0;
  } else {
    return RingBuffer_Remove(&rxBuffer);
  }
}

uint8_t wifiLineCount(void) {
  return lineCount;
}

// Called whenever a newline is inserted into the rx buffer.
void lineReady(void) {
  lineCount++;
}

// Receive complete
ISR(USART1_RX_vect) {
  uint8_t ch = UDRn;
  RingBuffer_Insert(&rxBuffer, ch);
  if (ch == '\r') {
    lineReady();
  }
}

uint8_t txIdle = 1;

void startSendBufferedChar(void) {
  UDRn = RingBuffer_Remove(&txBuffer);
  txIdle = 0;
}


// Transmit complete
ISR(USART1_TX_vect) {
  if (RingBuffer_IsEmpty(&txBuffer)) {
    txIdle = 1;
  } else {
    startSendBufferedChar();
  }
}

void sendChar(char ch) {
  while (RingBuffer_IsFull(&txBuffer)) {}
  RingBuffer_Insert(&txBuffer, ch);
  if (txIdle) {
    startSendBufferedChar();
  }
}
  
void sendStr(char *str) {
  char ch;
  while ((ch = *(str++))) {
    sendChar(ch);
  }
}

int uart_putchar(char c, FILE *stream){
  sendChar(c);
  return 0;
}

int uart_getchar(FILE *stream) {
  if (RingBuffer_IsEmpty(&rxBuffer)) {
    return _FDEV_EOF;
  }
  
  return RingBuffer_Remove(&rxBuffer);
}

void uartInit(void) {
  RingBuffer_InitBuffer(&txBuffer, txData, sizeof(txData));
  RingBuffer_InitBuffer(&rxBuffer, rxData, sizeof(rxData));
  RingBuffer_InitBuffer(&mixBuffer, mixData, sizeof(mixData));

  UCSRnA = _BV(U2Xn);   
  UBRRnH = 0;
  UBRRnL = (F_CPU / (8UL * UART_BAUD)) - 1;
  UCSRnB = _BV(TXENn) | _BV(RXENn) | _BV(RXCIEn) | _BV(TXCIEn); 
  
  static FILE uart_output = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
  static FILE uart_input  = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);
  
  stdout = &uart_output;
  stdin  = &uart_input;
}

void wifiInit(void) {
  uartInit();
  delayUs(1000);  
}

void wifiConfig(void) {
  
    //L("AT+RST\r\n");
  AT("E0");
  //AT("+GMR");
  //L("AT+GMR\r\n");
//  L("AT+CWMODE_CUR=1\r\n");
//  L("AT+CWJAP_CUR=\"Pretty Fly For A WIFI\",\"uno234556\"\r\n");
//  L("AT+CWDHCP_CUR=1,1\r\n");
//  L("AT+CWDHCPS_CUR?\r\n");
}

