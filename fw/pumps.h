#pragma once

#include "board.h"
#include <stdint.h>

void pumpsInit(void);

void pumpsCirculate(uint16_t millis);

void pumpsRun1(uint16_t millis);
void pumpsRun2(uint16_t millis);
