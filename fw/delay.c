#include "delay.h"

#include <util/delay.h>
#include <util/delay_basic.h>


void delayUs(uint16_t micro) {
  while (micro >= 48) {
    micro -= 48;
    _delay_loop_1(0);
  }
  micro--;
  _delay_loop_1(micro*5);
}
