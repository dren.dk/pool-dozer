# Pool-Dozer

This project is my prototype of a spa/pool water chemistry monitoring and regulation system.

The plan is to first implement monitoring and graphing via grafana, to avoid the manual testing, once that works reliably
adding clorine and acid dosing ought to be relatively easy.

## Chemistry

* Spa note: Do not regulate while jet pump is running, air increases pH temporarily
* First achive ideal pH of 7.2-7.6 (aim for 7.4)
* When pH is in the ideal range, try to hit 1-3 ppm free clorine (aim for 2 ppm)
* Alkalinity 80-120 ppm

## Frontend Construction

To protect the sensors and make the input reliable as possible I have floated the frontends with an isolated
DC/DC converter and optocouplers, this means that the analog ground is free to float to whatever voltage the water is at.

With completely floating inputs it's nolonger neccesaray to generate a negative voltage for the opamps, in stead the frontend input gnd
can be connected to the analog reference voltage and that will in turn ensure that the power supply will.


## Status

The current implementation is built with prototype modules on a perf-board, but that's very inefficient and unreliable.

One example of inefficiency is that the high-impedance frontend amplifier boards have the crazy-expensive LTC2420 ADCs when
cheaper and higher performance parts exist each frontend also has a voltage reference and a negative power supply, which should
be shared.

If the prototype works out then I might create an optimized single-board construction.




