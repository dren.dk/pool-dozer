# Pool dozer controller

The controller runs on the pi zero w and talks directly to the sensors and actuators via GPIO.

## Pinout

| Function      | GPIO | Pi pin |
|---------------|------|--------|
| +3.3V         |      | 1,17 |
| +5V           |      | 2,4 |
| GND           |      | 6,9,29,25,30,34,39 |
| ADC clock     |  0    | 11 |
| ADC CS pH     |  1    | 12 |
| ADC CS ORP    |  2    | 13 |
| ADC data in   |  3    | 15 |
| 1-wire data   |  4   | 16 |
| Sample pump   |  5   | 18 |
| Acid pump     |  6   | 22 |
| Chlorine pump |  7   | 7 |


![](https://pi4j.com/1.2/images/j8header-zero-large.png)

## 1-wire
Add to: /boot/config.txt

dtoverlay=w1-gpio-pullup,gpiopin=x,pullup=y


# High/Low level split

To make development easier, the low-level IO is handled by a simple server which contains only the parts of the code
needed to talk to the hardware, while everything else is handled by a high-level process which can run on a different host
during development.

During normal operation the low-level and high-level code can coexist in the same process.

# Integration with the balboa spa controller

Depending on how the sample pump is plumbed into the spa, it can be very useful to know the state of the spa circulation
pump.

In my case the sample pump takes in water from the filter, pipes it though the sample cell and then ejects it in the
injection manifold that's mounted just before the circulation pump outlet, this has two implications:

* I have to have the sample pump running whenever the circulation pump is running, if I want to sample the water before
the heater.
* I can only run the acid and oxidizer pumps when the circulation pump is running, otherwise a concentrated amount could
accumulate and be shot out when the circulation pump starts.
  
Happily my spa has a balboa spa controller (BP6013g1) which has the good grace to export its state on RS485 at 3.3Hz:
https://github.com/ccutrer/balboa_worldwide_app/wiki

# Control loop

* Idle while waiting for the circulation pump to start.
* Start the sample pump
* Wait 30 seconds for sensors to stabilize.
* Sample temperature, ORP and pH for 30 seconds
* If last dose of any kind > 120 seconds  
  * Update the acid flow rate according to measurement (ml/day)
  * Calculate the acid dose since last dose.
  * Dose acid
* If last dose of any kind > 120 seconds
  * Update the oxidizer flow rate according to measurements (ml/day)
  * Calculate the acid and oxidizer dose since last dose.
  * Dose oxidiser