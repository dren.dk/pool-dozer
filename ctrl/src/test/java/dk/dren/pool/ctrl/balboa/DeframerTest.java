package dk.dren.pool.ctrl.balboa;

import dk.dren.pool.ctrl.balboa.message.StatusUpdate;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class DeframerTest {

    @Test
    public void deframe() throws IOException {

        Map<Byte, Integer> typeCounts = new TreeMap<>();
        Deframer deframer = new Deframer(frame-> {
            typeCounts.compute(frame.getType(), (k, old)->old != null ? old +1 : 0);

            if (frame.getType() == StatusUpdate.ID) {
                final StatusUpdate statusUpdate = frame.parsePayload(StatusUpdate.class);
                System.out.println(statusUpdate);
            }
        });

        HexStreamer.streamHex("/balboa/stream.hex.gz", deframer::addByte);

        // See that we found the expected number of each frame type
        Assert.assertEquals(4, typeCounts.size());
        Assert.assertEquals(34, (int)typeCounts.get((byte)0));
        Assert.assertEquals(1596, (int)typeCounts.get((byte)6));
        Assert.assertEquals(1596, (int)typeCounts.get((byte)0x11));
        Assert.assertEquals(115, (int)typeCounts.get((byte)0x13));
    }

    @Test
    public void deframe2() throws IOException {

        Map<Byte, Integer> typeCounts = new TreeMap<>();
        Deframer deframer = new Deframer(frame-> {
            typeCounts.compute(frame.getType(), (k, old)->old != null ? old +1 : 0);

            if (frame.getType() == StatusUpdate.ID) {
                final StatusUpdate statusUpdate = frame.parsePayload(StatusUpdate.class);
                System.out.println(statusUpdate);
            }
        });

        HexStreamer.streamHex("/balboa/stream3.hex.gz", deframer::addByte);

        // See that we found the expected number of each frame type

        Assert.assertEquals(4, typeCounts.size());
        /*
        Assert.assertEquals(34, (int)typeCounts.get((byte)0));
        Assert.assertEquals(1596, (int)typeCounts.get((byte)6));
        Assert.assertEquals(1596, (int)typeCounts.get((byte)0x11));
        Assert.assertEquals(115, (int)typeCounts.get((byte)0x13));

         */
    }



}