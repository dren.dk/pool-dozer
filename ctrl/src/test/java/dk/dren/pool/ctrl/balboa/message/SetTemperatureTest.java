package dk.dren.pool.ctrl.balboa.message;

import dk.dren.pool.ctrl.balboa.Deframer;
import dk.dren.pool.ctrl.balboa.Frame;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

class SetTemperatureTest {

    @Test
    void toFrame() {
        final SetTemperature setTemperature = new SetTemperature(47);

        AtomicBoolean found = new AtomicBoolean(false);
        final Deframer deframer = new Deframer(f -> {
            final SetTemperature parsed = f.parsePayload(SetTemperature.class);
            Assertions.assertEquals(setTemperature.getTemperatureInC(), parsed.getTemperatureInC());
            found.set(true);
        });

        final Frame generated = setTemperature.toFrame();
        for (byte b : generated.toBytes()) {
            deframer.addByte(b);
        }

        Assertions.assertTrue(found.get());
    }

}