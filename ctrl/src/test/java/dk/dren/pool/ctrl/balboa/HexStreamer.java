package dk.dren.pool.ctrl.balboa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * Test utility that reads a compressed stream of bytes encoded as whitespace-separated hex bytes and then gzipped and
 * placed on the classpath.
 */
public class HexStreamer {
    public static void streamHex(String classpathHexGz, Consumer<Byte> byteConsumer) throws IOException {

        final Pattern hexByte = Pattern.compile("([a-f0-9]{2})");
        try (final InputStream resourceAsStream = HexStreamer.class.getResourceAsStream(classpathHexGz)) {
            if (resourceAsStream == null) {
                throw new IllegalArgumentException("Could not load "+classpathHexGz+" from classpath");
            }
            final BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(resourceAsStream)));
            int lineNumber = 1;
            while (true) {
                final String line = br.readLine();
                if (line == null) {
                    return;
                }

                int byteNumber = 1;
                final Matcher byteMatcher = hexByte.matcher(line);
                while (byteMatcher.find()) {
                    final byte b = (byte) (0xff & Integer.parseInt(byteMatcher.group(1), 16));
                    try {
                        byteConsumer.accept(b);
                    } catch (Exception e) {
                        throw new RuntimeException(String.format("Failed at line %d, byte %d",lineNumber, byteNumber), e);
                    }
                }
            }
        }
    }
}
