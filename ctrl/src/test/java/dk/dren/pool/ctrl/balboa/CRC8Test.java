package dk.dren.pool.ctrl.balboa;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CRC8Test {
    @Test
    public void test() {
        // 7e 05 10 bf 06 5c 7e
        // Sentinel
        //    Length
        //       Destination
        //          Garbage
        //             Type
        //                CRC
        //                   Sentinel

        CRC8 crc8 = new CRC8();

        crc8.reset();
        crc8.update((byte)0x05);
        crc8.update((byte)0x10);
        crc8.update((byte)0xbf);
        crc8.update((byte)0x06);

        Assert.assertEquals(0x5c, crc8.getCRC8());
    }

}