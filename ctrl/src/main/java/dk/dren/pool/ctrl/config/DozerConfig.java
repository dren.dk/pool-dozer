package dk.dren.pool.ctrl.config;

import io.dropwizard.Configuration;
import lombok.Data;

@Data
public class DozerConfig extends Configuration {

    IOConfig io = new IOConfig();

    ControllerConfig controller = new ControllerConfig();

    InfluxConfig influx = new InfluxConfig();
}
