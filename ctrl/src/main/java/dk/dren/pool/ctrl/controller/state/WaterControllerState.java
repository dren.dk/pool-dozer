package dk.dren.pool.ctrl.controller.state;

import dk.dren.pool.ctrl.controller.WaterController;

public interface WaterControllerState {
    //static void transition(WaterController waterController);
    void poll(WaterController waterController);
}
