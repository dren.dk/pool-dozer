package dk.dren.pool.ctrl.io;

public interface Pump {
    PumpName getName();
    void run();
    void run(int ms);
    void stop();
    boolean isRunning();
}
