package dk.dren.pool.ctrl.config;

import lombok.Data;

@Data
public class ControllerConfig {
    /**
     * Enable the control loop or just provide the interface to the io.
     */
    private boolean enabled = false;

    /**
     * The number of seconds between polling the control loop
     */
    private int interval = 2;

    /**
     * The number of seconds to run the sample pump after the circulation pump starts and jets stop before reading the sensors
     */
    private int sample = 600;

    private boolean alwaysRunSamplePump = true;

    /**
     * The ADC configuration for the pH input
     */
    private ADCConfig ph = ADCConfig.builder()
            .zero(524358)
            .slope(418863)
            .averagingWeight(0.1)
            .build();

    /**
     * The ADC configuration for the ORP input
     */
    private ADCConfig orp = ADCConfig.builder()
            .zero(524658)
            .slope(419510)
            .averagingWeight(0.1)
            .build();

    /**
     * The ID of the water temperature sensor
     */
    private String waterTemperatureId = "28-0417c10c12ff";



}
