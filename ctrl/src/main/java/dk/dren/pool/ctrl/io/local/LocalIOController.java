package dk.dren.pool.ctrl.io.local;

import dk.dren.pool.ctrl.api.IOState;
import dk.dren.pool.ctrl.balboa.BalboaSerialClient;
import dk.dren.pool.ctrl.io.IOController;
import dk.dren.pool.ctrl.io.Pump;
import dk.dren.pool.ctrl.io.PumpName;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Controls hardware connected to the host it runs on, iow. this class goes on the pi.
 */
@RequiredArgsConstructor
public class LocalIOController implements IOController {
    public static final int PH_ADC_INDEX = 0;
    public static final int ORP_ADC_INDEX = 1;

    private final W1TemperatureSensors w1TemperatureSensors = new W1TemperatureSensors();
    private final LTC2420 ltc2420;
    private final Map<PumpName, Pump> pumps = new TreeMap<>();
    private BalboaSerialClient balboaSerialStatusReceiver;

    public LocalIOController(LTC2420 ltc2420, LocalPump samplePump, LocalPump acidPump, LocalPump oxidizerPump, File balboaRS485Port) {
        this.ltc2420 = ltc2420;
        pumps.put(PumpName.SAMPLE, samplePump);
        pumps.put(PumpName.ACID, acidPump);
        pumps.put(PumpName.OXIDIZER, oxidizerPump);

        if (balboaRS485Port != null) {
            balboaSerialStatusReceiver = new BalboaSerialClient(balboaRS485Port);
        }
    }

    @Override
    public IOState getIOState() throws IOException, InterruptedException {
        return new IOState(
            ltc2420.read(PH_ADC_INDEX),
            ltc2420.read(ORP_ADC_INDEX),
            w1TemperatureSensors.readAllTemperatures(),
            pumps.get(PumpName.SAMPLE).isRunning(),
            pumps.get(PumpName.ACID).isRunning(),
            pumps.get(PumpName.OXIDIZER).isRunning(),
            balboaSerialStatusReceiver != null ? balboaSerialStatusReceiver.getStatusUpdate() : null
        );
    }

    @Override
    public Map<PumpName, Pump> getPumpByName() {
        return pumps;
    }
}
