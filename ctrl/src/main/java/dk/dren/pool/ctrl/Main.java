package dk.dren.pool.ctrl;

public class Main {
	public static void main(String[] args) {
        try {
        	new DozerApp().run(args);
        } catch (Throwable t) {
        	System.err.println("Failed while starting the application, giving up");
        	t.printStackTrace(System.err);
        	System.exit(254);
        }
    }
}
