package dk.dren.pool.ctrl.io.remote;

import dk.dren.pool.ctrl.api.IOState;
import dk.dren.pool.ctrl.io.IOController;
import dk.dren.pool.ctrl.io.Pump;
import dk.dren.pool.ctrl.io.PumpName;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.TreeMap;

/**
 * Controls hardware over the network, via the REST interface exposed by IOResource
 */
public class RemoteIOController implements IOController {
    private final RemoteIOService remoteIOService;
    private final Map<PumpName, Pump> pumps = new TreeMap<>();

    public RemoteIOController(URI remote) {
        String baseUrl = remote.toString();
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        remoteIOService = retrofit.create(RemoteIOService.class);

        for (PumpName pumpName : PumpName.values()) {
            pumps.put(pumpName, new RemotePump(remoteIOService, pumpName));
        }
    }

    @Override
    public IOState getIOState() throws IOException {
        return remoteIOService.getState().execute().body();
    }

    @Override
    public Map<PumpName, Pump> getPumpByName() {
        return pumps;
    }
}
