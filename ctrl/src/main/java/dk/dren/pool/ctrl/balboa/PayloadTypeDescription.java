package dk.dren.pool.ctrl.balboa;

import dk.dren.pool.ctrl.balboa.message.NewClientCTS;
import dk.dren.pool.ctrl.balboa.message.Payload;
import dk.dren.pool.ctrl.balboa.message.SetTemperature;
import dk.dren.pool.ctrl.balboa.message.StatusUpdate;
import lombok.Value;

import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

/**
 * Describes a message type and ties the message type ID to the constructor of the payload parser
 */
@Value
public class PayloadTypeDescription {
    byte id;
    String name;
    Function<Frame, Payload> parser;

    @Override
    public String toString() {
        return String.format("%s (%02x)", name, id);
    }

    public static PayloadTypeDescription get(byte id) {
        return TYPE_TO_DESCRIPTION.get(id);
    }

    public static final Map<Byte, PayloadTypeDescription> TYPE_TO_DESCRIPTION = new TreeMap<>();
    private static void add(int id, String name, Function<Frame, Payload> parser) {
        final PayloadTypeDescription ptd = new PayloadTypeDescription((byte)id, name, parser);
        TYPE_TO_DESCRIPTION.put(ptd.getId(), ptd);
    }

    static {
        add(0x00, "?Settings 0x10 Response?", null);
        add(NewClientCTS.ID, "New Client Clear to Send", NewClientCTS::new);
        add(0x01, "Channel Assignment Request", null);
        add(0x02, "Channel Assignment Response", null);
        add(0x03, "Channel Assignment Acknowledgement", null);
        add(0x04, "Existing Client Request", null);
        add(0x05, "Existing Client Response", null);
        add(0x06, "Clear to Send", null);
        add(0x07, "Nothing to Send", null);
        add(0x11, "Toggle Item Request", null);
        add(0x12, "??", null);
        add(StatusUpdate.ID, "Status Update", StatusUpdate::new);
        add(0x14, "??", null);
        add(SetTemperature.ID, "Set Temperature Request", SetTemperature::new);
        add(0x21, "Set Time Request", null);
        add(0x22, "Settings Request", null);
        add(0x23, "Filter Cycles Message", null);
        add(0x24, "Information Response", null);
        add(0x25, "?Settings 0x04 Response", null);
        add(0x26, "Preferences Response", null);
        add(0x27, "Set Preference Request", null);
        add(0x28, "Fault Log Response", null);
        add(0x29, "?Settings 0x40 Response?", null);
        add(0x2A, "Change Setup Request", null);
        add(0x2B, "GFCI Test Response", null);
        add(0x2D, "Lock Request", null);
        add(0x2E, "Configuration Response", null);
        add(0x48, "??", null);
        add(0x82, "??", null);
        add(0x92, "Set WiFi Settings Request", null);
        add(0x94, "WiFi Module Configuration Response", null);
        add(0xE0, "Toggle Test Setting Request", null);
        add(0xE1, "?Error?", null);
        add(0xF0, "?Error?", null);
    }
}
