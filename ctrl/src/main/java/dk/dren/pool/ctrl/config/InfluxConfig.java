package dk.dren.pool.ctrl.config;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;

import java.net.URI;

@Data
@ToString(doNotUseGetters = true)
public class InfluxConfig {
    URI uri;
    String user = "spa";
    String password;
    String database = "spa";
    String location = "spa";

    @Getter(lazy = true)
    private final InfluxDB influxDB = connect();

    private InfluxDB connect() {
        if (uri == null) {
            return null;
        } else {
            final InfluxDB db = InfluxDBFactory.connect(uri.toString(), user, password);
            db.setDatabase(database);
            return db;
        }
    }
}
