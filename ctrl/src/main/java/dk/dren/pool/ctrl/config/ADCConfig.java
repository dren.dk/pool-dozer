package dk.dren.pool.ctrl.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ADCConfig {

    /**
     * The ADC value that corresponds to 0V at the input.
     */
    private double zero;

    /**
     * The ADC count per Volt
     */
    private double slope;

    /**
     * The weight of the delta compared to the moving average
     */
    @Max(1)
    private double averagingWeight;
}
