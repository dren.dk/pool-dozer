package dk.dren.pool.ctrl.io;

public enum PumpName {
    SAMPLE,
    ACID,
    OXIDIZER
}
