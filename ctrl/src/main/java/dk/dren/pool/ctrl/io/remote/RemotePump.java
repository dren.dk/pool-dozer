package dk.dren.pool.ctrl.io.remote;

import dk.dren.pool.ctrl.io.Pump;
import dk.dren.pool.ctrl.io.PumpName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.IOException;

/**
 * Controls a pump over the network, via the REST interface exposed by PumpResource
 */
@RequiredArgsConstructor
public class RemotePump implements Pump {
    private final RemoteIOService remoteIOService;
    @Getter
    private final PumpName name;

    @Override
    public void run() {
        try {
            remoteIOService.runPump(name.name()).execute();
        } catch (IOException e) {
            throw new RuntimeException("Failed to make call to remote pump "+name);
        }
    }

    @Override
    public void run(int ms) {
        try {
            remoteIOService.runPump(name.name(), ms).execute();
        } catch (IOException e) {
            throw new RuntimeException("Failed to make call to remote pump "+name);
        }
    }

    @Override
    public void stop() {
        try {
            remoteIOService.stopPump(name.name()).execute();
        } catch (IOException e) {
            throw new RuntimeException("Failed to make call to remote pump "+name);
        }
    }

    @Override
    public boolean isRunning() {
        try {
            return remoteIOService.isPumpRunning(name.name()).execute().body();
        } catch (IOException e) {
            throw new RuntimeException("Failed to make call to remote pump "+name);
        }
    }
}
