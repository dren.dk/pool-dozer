package dk.dren.pool.ctrl.balboa.message;

import dk.dren.pool.ctrl.balboa.Frame;

public interface SendablePayload extends Payload {
    /**
     * Encode the payload as a frame
     */
    Frame toFrame();
}
