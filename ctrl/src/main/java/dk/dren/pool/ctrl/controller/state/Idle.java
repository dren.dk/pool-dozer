package dk.dren.pool.ctrl.controller.state;

import dk.dren.pool.ctrl.api.IOState;
import dk.dren.pool.ctrl.controller.WaterController;
import dk.dren.pool.ctrl.io.Pump;
import dk.dren.pool.ctrl.io.PumpName;

import java.util.concurrent.TimeUnit;

public class Idle implements WaterControllerState {
    public static final Idle INSTANCE = new Idle();

    public static void transition(WaterController waterController) {

        final boolean alwaysRunSamplePump = waterController.getConfiguration().getController().isAlwaysRunSamplePump();
        for (Pump pump : waterController.getConfiguration().getIo().getIoController().getPumpByName().values()) {
            if (pump.getName().equals(PumpName.SAMPLE) && alwaysRunSamplePump) {
                if (!pump.isRunning()) {
                    pump.run();
                }

            } else {
                if (pump.isRunning()) {
                    pump.stop();
                }
            }
        }

        waterController.setState(INSTANCE);
    }

    public void poll(WaterController waterController) {
        final IOState ioState = waterController.getInputs().getIoState();
        if (ioState.getBalboaStatusUpdate().isCirculationPumpRunning()) {

            // We always run the sample pump
            runSamplePump(waterController);

            // But we cannot progress if the jet pump or any of the dosing pumps is currently running.
            if (ioState.isOxidizerPumpRunning() || ioState.isAcidPumpRunning() || ioState.getBalboaStatusUpdate().getPump1() != 0) {
                // Waiting for pumps to idle.
            } else {
                final long millis = TimeUnit.SECONDS.toMillis(waterController.getConfiguration().getController().getSample());
                Pause.transition(waterController, millis, new UpdateChemistry(waterController));
            }
        }
    }

    private static void runSamplePump(WaterController waterController) {
        waterController.getConfiguration().getIo().getIoController().getPumpByName().get(PumpName.SAMPLE).run();
    }

    @Override
    public String toString() {
        return "Idle";
    }
}
