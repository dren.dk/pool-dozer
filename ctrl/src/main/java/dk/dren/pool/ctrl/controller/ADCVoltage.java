package dk.dren.pool.ctrl.controller;

import dk.dren.pool.ctrl.config.ADCConfig;
import lombok.Getter;
import lombok.extern.java.Log;

/**
 * Convert from raw ADC values to a Voltage, with:
 * * Zero point calibration
 * * Slope calibration
 * * Moving average
 */
@Log
public class ADCVoltage {
    private final ADCConfig config;

    @Getter
    private double rawValue;

    public ADCVoltage(ADCConfig config, long firstSample) {
        this.config = config;
        rawValue = firstSample;
    }

    public void update(long sample) {
        final double rawDelta = sample - rawValue;
        rawValue += rawDelta * config.getAveragingWeight();
    }

    public double getValue() {
        return adcToVolt(rawValue);
    }

    private double adcToVolt(double sample) {
        return (sample - config.getZero()) / config.getSlope();
    }

    @Override
    public String toString() {
        return ((long)Math.round(getValue()*1000))+" mV (raw: "+getRawValue()+")";
    }
}
