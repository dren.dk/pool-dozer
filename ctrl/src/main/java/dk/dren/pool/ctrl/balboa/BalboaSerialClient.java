package dk.dren.pool.ctrl.balboa;

import dk.dren.pool.ctrl.balboa.message.ClearToSend;
import dk.dren.pool.ctrl.balboa.message.NewClientCTS;
import dk.dren.pool.ctrl.balboa.message.SetTemperature;
import dk.dren.pool.ctrl.balboa.message.StatusUpdate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

/**
 * A very simplified RS485 receiver for the balboa spa controller protocol, that does only one thing:
 * Listens for status update messages and decodes them at a minimum interval to avoid spamming the heap
 * with a lot of decoded status update objects that are never going to be used.
 *
 * Only the last StatusUpdate is being kept and can always be retrieved with the getStatusUpdate() method.
 */
@Slf4j
public class BalboaSerialClient {
    public static final int DECODE_INTERVAL = 5000;

    @Getter
    private final BalboaSerial balboaSerial;
    @Getter
    private long lastStatusUpdate = 0;
    private StatusUpdate statusUpdate;
    private double targetTemp = 25;

    public BalboaSerialClient(File serialPort) {
        balboaSerial = new BalboaSerial(serialPort, this::handleFrame);
    }

    private void handleFrame(Frame frame) {
        if (frame.getType() == ClearToSend.ID) {
            /*
            synchronized (this) {
                if (statusUpdate != null) {
                    if (Math.abs(statusUpdate.getSetTemperatureCelsius() - targetTemp) > 0.1) {
                        log.info("Current setpoint: {} changing to: {}",
                                statusUpdate.getSetTemperatureCelsius(),
                                targetTemp);
                        balboaSerial.send(new SetTemperature(targetTemp));
                    }
                }
            }
            */
        } else if (frame.getType() == StatusUpdate.ID) {
            final long now = System.currentTimeMillis();
            if (now-lastStatusUpdate > DECODE_INTERVAL) {
                lastStatusUpdate = now;
                final StatusUpdate newStatusUpdate = frame.parsePayload(StatusUpdate.class);
                synchronized (this) {
                    this.statusUpdate = newStatusUpdate;
                }
                log.debug("status: {}", statusUpdate);
            }
        }
    }

    public synchronized StatusUpdate getStatusUpdate() {
        return statusUpdate;
    }
}
