package dk.dren.pool.ctrl.balboa;

import dk.dren.pool.ctrl.balboa.message.Payload;
import lombok.Getter;

import java.util.function.Supplier;

/**
 * This is the raw information known about a frame of balboa data
 *
 * | Byte | Name | Description/Values |
 * | --- | --- | --- |
 * | 0 | Delimiter | 0x7E |
 * | 1 | Length | N - 2 (minimum of 5) |
 * | 2 | Channel | (see [Channels](#channels)) |
 * | 3 | ?? | 0xAF when Byte 2 is 0xFF; 0xBF otherwise |
 * | 4 | Type Code | (see [Message Types](#message-types)) |
 * | 5 to N - 2 | Arguments | Optional, usually the same for each message type |
 * | N - 1 | Checksum | CRC-8 of bytes 1 through N-2, polynomial=0x07, initial=0x02, final XOR=0x02 |
 * | N | Delimiter | 0x7E |
 */
public class Frame {
    @Getter
    private final byte channel;
    @Getter
    private final byte type;

    private Supplier<byte[]> payloadSupplier;
    private byte[] payload;

    public Frame(byte channel, byte type, Supplier<byte[]> payloadSupplier) {
        this.channel = channel;
        this.type = type;
        this.payloadSupplier = payloadSupplier;
    }

    public Frame(byte channel, byte type, byte... payload) {
        this.channel = channel;
        this.type = type;
        this.payload = payload;
    }

    public byte[] getPayload() {
        if (payload == null) {
            payload = payloadSupplier.get();
        }
        return payload;
    }

    /**
     * Gets the description of the payload, without actually parsing it.
     * @return The description of the payload or null if it's an unknown type
     */
    public PayloadTypeDescription getDescription() {
        return PayloadTypeDescription.get(type);
    }

    /**
     * Parses the payload, if possible.
     * @return An instance of Payload or null
     */
    public Payload parsePayload() {
        final PayloadTypeDescription description = getDescription();
        if (description == null) {
            throw new IllegalArgumentException(String.format("Unknown message type: %02x", type));
        }
        if (description.getParser() == null) {
            throw new IllegalArgumentException("No parser for "+description);
        }
        return description.getParser().apply(this);
    }

    public <T extends Payload> T parsePayload(Class<T> theClass) {
        final Payload payload = parsePayload();
        if (theClass.isAssignableFrom(payload.getClass())) {
            return (T)payload;
        } else {
            throw new IllegalArgumentException("Wrong type "+payload.getClass()+" is not "+theClass);
        }
    }

    @Override
    public String toString() {
        final PayloadTypeDescription description = getDescription();
        return String.format("Frame: channel:%02x, type:%02x (%s), payload:%s", channel, type, description != null ? description.getName() : "?", arrayToString());
    }

    private String arrayToString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%d bytes:", getPayload().length));
        for (int i = 0; i < getPayload().length; i++) {
            sb.append(String.format(" %02x", getPayload(i)));
        }
        return sb.toString();
    }

    public byte getPayload(int byteIndex) {
        return getPayload()[byteIndex];
    }

    public boolean bit(int byteIndex, int bit) {
        return (getPayload(byteIndex) & (1<<bit)) != 0;
    }

    public byte bits(int byteIndex, int bitIndex, int bitCount) {
        return (byte)((getPayload(byteIndex) >> bitIndex) & ((1<<bitCount) -1));
    }

    public byte[] toBytes() {
        final byte[] payload = getPayload();
        byte[] b = new byte[payload.length+7];
        CRC8 crc = new CRC8();

        b[0] = Deframer.SENTINEL;
        crc.update(b[1] = (byte)(payload.length+5));
        crc.update(b[2] = channel);
        crc.update(b[3] = (byte)(channel == 0xff ? 0xaf : 0xbf));
        crc.update(b[4] = type);

        for (int i=0;i<payload.length;i++) {
            crc.update(b[5+i] = payload[i]);
        }

        b[payload.length+5] = crc.getCRC8();
        b[payload.length+6] = Deframer.SENTINEL;

        return b;
    }

}
