package dk.dren.pool.ctrl.api;

import dk.dren.pool.ctrl.balboa.message.StatusUpdate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * The raw io state, without any corrections or calibrations.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IOState {
    private long phADC;
    private long orpADC;
    private Map<String, Float> temperatures;
    private boolean samplePumpRunning;
    private boolean acidPumpRunning;
    private boolean oxidizerPumpRunning;
    private StatusUpdate balboaStatusUpdate;
}
