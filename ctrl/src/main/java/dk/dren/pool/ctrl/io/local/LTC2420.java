package dk.dren.pool.ctrl.io.local;

import com.diozero.util.SleepUtil;
import com.pi4j.io.gpio.*;
import dk.dren.pool.ctrl.hack.HackItUntilItWorks;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Reads from a number of LTC2420 ADCs via GPIO
 */
@Slf4j
public class LTC2420 {
    private static final int EOC = 1<<23;
    private static final int DMY = 1<<22;
    private static final int SIG = 1<<21;
    private static final int EXR = 1<<20;

    private final GpioPinDigitalOutput clock;
    private final GpioPinDigitalInput data;
    private final List<GpioPinDigitalOutput> chipSelect;

    public LTC2420(Pin clock, Pin data, Pin... chipSelect) {
        final GpioController gpio = GpioFactory.getInstance();
        this.clock = HackItUntilItWorks.run(()->gpio.provisionDigitalOutputPin(clock));
        this.data = HackItUntilItWorks.run(()->gpio.provisionDigitalInputPin(data));
        this.chipSelect = Arrays.stream(chipSelect)
                .map(p->gpio.provisionDigitalOutputPin(p, PinState.HIGH))
                .collect(Collectors.toList());
    }

    public long read(int index) {

        // Select the right chip
        deselectAllChips();
        chipSelect.get(index).low();

        sleepTick();

        // Wait for ~EOC state
        int patience = 1000;
        while (data.isHigh() && patience-->0) {
            sleepTick();
        }

        if (patience < 0) {
            log.warn("Time out waiting for End Of Conversion for ADC {}", index);
        }

        long data = 0;
        byte bitsToGo = 24;
        while (bitsToGo-- > 0) {
            data <<= 1;

            clock.high();
            sleepTick();
            final boolean high = this.data.isHigh();
            clock.low();
            sleepTick();

            if (high) {
                data |= 1;
            }
        }

        deselectAllChips();

        if ((EOC & data) != 0) {
            throw new IllegalStateException("ADC timeout: "+data);
        }

        if ((DMY & data) != 0) {
            throw new IllegalStateException("ADC bad: "+data);
        }

        if ((EXR & data) != 0) {
            if ((SIG & data) != 0) {
                throw new IllegalStateException("Over range: "+data);
            } else {
                throw new IllegalStateException("Under range: "+data);
            }
        }

        return data & 0xfffff;
    }

    private void deselectAllChips() {
        for (GpioPinDigitalOutput cs : chipSelect) {
            cs.high();
        }
    }

    private void sleepTick() {
        SleepUtil.sleepMicros(50);
    }

}
