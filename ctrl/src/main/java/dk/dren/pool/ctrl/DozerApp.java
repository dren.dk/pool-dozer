package dk.dren.pool.ctrl;

import dk.dren.pool.ctrl.config.DozerConfig;
import dk.dren.pool.ctrl.controller.WaterController;
import dk.dren.pool.ctrl.resources.IOResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class DozerApp extends Application<DozerConfig> {
    @Override
    public String getName() {
        return "Pool Dozer";
    }

    @Override
    public void initialize(Bootstrap<DozerConfig> bootstrap) {

    }

    @Override
    public void run(DozerConfig configuration, Environment environment) throws Exception {
        environment.jersey().register(new IOResource(configuration.getIo().getIoController()));
        if (configuration.getController().isEnabled()) {
            final WaterController waterController = new WaterController(configuration);
            environment.lifecycle().manage(waterController);
        }
    }
}
