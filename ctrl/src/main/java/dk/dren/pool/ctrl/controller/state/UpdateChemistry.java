package dk.dren.pool.ctrl.controller.state;

import dk.dren.pool.ctrl.controller.WaterController;
import lombok.extern.java.Log;

import java.util.concurrent.TimeUnit;

@Log
public class UpdateChemistry implements WaterControllerState {
    public UpdateChemistry(WaterController wc) {

    }

    @Override
    public void poll(WaterController waterController) {
        final long quietMillis = TimeUnit.SECONDS.toMillis(waterController.getConfiguration().getController().getSample());
        final long timeSinceAcidDose = System.currentTimeMillis()-waterController.getInputs().getLastAcidDose();
        if (timeSinceAcidDose < quietMillis) {
            log.info("The last acid dose was "+ timeSinceAcidDose +" ms ago, it must be at least "+quietMillis+" ms between dosing and updating chemistry");
            Idle.transition(waterController);
            return;
        }
        final long timeSinceOxidizerDose = System.currentTimeMillis()-waterController.getInputs().getLastOxidizerDose();
        if (timeSinceOxidizerDose < quietMillis) {
            log.info("The last oxidizer dose was "+ timeSinceOxidizerDose +" ms ago, it must be at least "+quietMillis+" ms between dosing and updating chemistry");
            Idle.transition(waterController);
            return;
        }
        final long timeSinceJets = System.currentTimeMillis()-waterController.getInputs().getLastJet();
        if (timeSinceJets < quietMillis) {
            log.info("The last time the jets were used was "+ timeSinceJets +" ms ago, it must be at least "+quietMillis+" ms between jets and updating chemistry");
            Idle.transition(waterController);
            return;
        }

        // Ok, now that's over with, we know that the samples were getting from the sensors will be valid.
        // TODO: Update the chemistry parameters and persist them

    }

    @Override
    public String toString() {
        return "Update Chemistry";
    }
}
