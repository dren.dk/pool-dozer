package dk.dren.pool.ctrl;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import dk.dren.pool.ctrl.balboa.BalboaSerialClient;
import dk.dren.pool.ctrl.balboa.message.StatusUpdate;

import java.io.File;

public class RS485 {
    public static void main(String[] args) {
        try {
            if (args.length == 1 && args[0].equals("dump")) {
                final SerialPort commPort = SerialPort.getCommPort("/dev/ttyUSB0");
                commPort.setBaudRate(115200);
                commPort.setNumDataBits(8);
                commPort.setParity(SerialPort.NO_PARITY);
                commPort.setNumStopBits(1);
                //commPort.setRs485ModeParameters(true, true, 0, 0);
                System.err.println("Opened port: "+commPort.openPort());

                commPort.addDataListener(new SerialPortDataListener() {
                    int byteCount = 0;
                    int lastPrint = 0;
                    @Override
                    public int getListeningEvents() {
                        return SerialPort.LISTENING_EVENT_DATA_AVAILABLE | SerialPort.LISTENING_EVENT_DATA_RECEIVED;
                    }

                    byte prev;
                    @Override
                    public void serialEvent(SerialPortEvent event) {
                        for (byte data : event.getReceivedData()) {
                            System.out.printf(" %02x", data);
                            if (data == 0x7e && prev != 0x7e) {
                                System.out.println(); // This will insert a newline after each frame, or thereabouts.
                            }
                            prev = data;
                        }
                        byteCount += event.getReceivedData().length;
                        if (byteCount-lastPrint > 1000) {
                            System.err.println("Recevied "+byteCount+" bytes");
                            lastPrint = byteCount;
                        }

                    }
                });
                while (true) {
                    Thread.sleep(1000);
                }
            } else {
                BalboaSerialClient statusReceiver = new BalboaSerialClient(new File("/dev/ttyUSB0"));
                while (true) {
                    final StatusUpdate statusUpdate = statusReceiver.getStatusUpdate();
                    if (statusUpdate != null) {
                        System.out.println(statusUpdate);
                    }
                    Thread.sleep(1000);
                }
            }

        } catch (Throwable t) {
            System.err.println("Failed while starting the application, giving up");
            t.printStackTrace(System.err);
            System.exit(254);
        }
    }

}
