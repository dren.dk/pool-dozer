package dk.dren.pool.ctrl.balboa.message;

import dk.dren.pool.ctrl.balboa.Frame;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Status update payload parsed out as much as possible.
 * https://github.com/ccutrer/balboa_worldwide_app/wiki#status-update
 */
@Data
@NoArgsConstructor
public class StatusUpdate implements Payload {
    public static final int ID = 0x13;

    /**
     * True of the spa is configured to use Celsius, false if using freedom units
     * Note that all temperatures are converted to Celsius in the fields of this class.
     */
    private boolean celsius;
    private boolean clock24h;
    private boolean filterCycle1;
    private boolean filterCycle2;
    private boolean panelLocked;
    private boolean temperatureRangeHigh;
    private boolean needsHeat;
    private boolean heating;
    private boolean heatWaiting;
    private byte pump1;
    private byte pump2;
    private byte pump3;
    private byte pump4;
    private byte pump5;
    private byte pump6;
    private boolean circulationPumpRunning;
    private byte blowerStatus;
    private byte lightStatus1;
    private byte lightStatus2;
    private boolean reminder;
    private boolean warningSettingsReset;
    private byte cleaningCycle;
    private boolean notification;
    private boolean sensorABtemperatures;
    private boolean timeout8h;
    private boolean settingsLocked;
    private byte timeHour;
    private byte timeMinute;
    private byte heatingMode;
    private byte reminderType;
    private double sensorACelsius;
    private double sensorBCelsius;
    private boolean misterOn;
    private double setTemperatureCelsius;
    private double waterTemperatureCelsius;

    public StatusUpdate(Frame frame) {

        // Flag byte 9
        celsius = frame.bit(9, 0);
        clock24h = frame.bit(9, 1);
        filterCycle1 = frame.bit(9, 3);
        filterCycle2 = frame.bit(9, 4);
        panelLocked = frame.bit(9, 5);

        // Flag byte 10
        temperatureRangeHigh = frame.bit(10, 2);
        needsHeat = frame.bit(10, 3);
        heating = frame.bit(10, 4);
        heatWaiting = frame.bit(10, 5);

        // Flag byte 11
        pump1 = frame.bits(11, 0, 2);;
        pump2 = frame.bits(11, 2, 2);
        pump3 = frame.bits(11, 4, 2);
        pump4 = frame.bits(11, 6, 2);

        // Flag byte 12
        pump5 = frame.bits(12, 0, 2);
        pump6 = frame.bits(12, 2, 2);

        // Flag byte 13
        circulationPumpRunning = frame.bit(13, 1);
        blowerStatus = frame.bits(13, 2, 2);

        // Flag byte 14
        lightStatus1 = frame.bits(14, 0, 2);
        lightStatus2 = frame.bits(14, 2, 2);

        // Flag byte 18
        reminder = frame.bit(18, 0);
        warningSettingsReset = frame.bit(18, 2);

        // Flag byte 19
        cleaningCycle = frame.bits(19, 0, 3);
        notification = frame.bit(19, 5);

        // Flag byte 21
        sensorABtemperatures = frame.bit(21, 1);
        timeout8h = frame.bit(21, 2);
        settingsLocked = frame.bit(21, 3);

        waterTemperatureCelsius = temperatureInCelsius(frame, 2);

        timeHour = frame.getPayload(3);
        timeMinute = frame.getPayload(4);
        heatingMode = frame.getPayload(5);
        reminderType = frame.getPayload(6);

        sensorACelsius = temperatureInCelsius(frame, 7);
        sensorBCelsius = temperatureInCelsius(frame, 8);

        misterOn = frame.bit(15, 0);

        setTemperatureCelsius = temperatureInCelsius(frame, 20);
    }

    private double temperatureInCelsius(Frame frame, int byteIndex) {
        double temp = frame.getPayload()[byteIndex];
        if (celsius) {
            return temp / 2;
        } else {
            return (temp - 32) / 1.8;
        }
    }



}
