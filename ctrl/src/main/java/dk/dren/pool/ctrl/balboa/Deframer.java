package dk.dren.pool.ctrl.balboa;


import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * Decodes the output of a Balboa spa controller, see:
 * https://github.com/ccutrer/balboa_worldwide_app/wiki
 * or balboa-wiki.md
 *
 * This class handles turning the stream of bytes on RS485 into validated frames of bytes
 * that can then be handed to a payload parser.
 *
 * Each message consists of a frame wrapped around a payload:
 * sentinel + length + channel + afbf + payloadType + payloadBytes + crc + sentinel
 *
 * The frame is handled by this class, it outputs a validated Frame object
 * which can then be used to access the parsed payload.
 */
@Slf4j
public class Deframer {
    /**
     * This byte bookends each frame
     */
    public static final int SENTINEL = 0x7e;
    public static final int MAX_PAYLOAD_SIZE = 120;
    public static final int MIN_PAYLOAD_SIZE = 5;
    private final Consumer<Frame> frameConsumer;

    private int messageLength;
    private int bytesLeftInMessage;
    private byte[] payload = new byte[MAX_PAYLOAD_SIZE];
    private byte payloadInUse = 0;
    private CRC8 crc = new CRC8();

    /**
     * This always points to the code that will consume the next byte of the stream
     */
    private Consumer<Byte> state;

    /**
     * Creates a new deframer
     *
     * @param frameConsumer Consumer that's handed all the parsed frames
     */
    public Deframer(Consumer<Frame> frameConsumer) {
        this.frameConsumer = frameConsumer;
        startLookingForMessageStart();
    }

    private void startLookingForMessageStart() {
        log.debug("Looking for start-sentinel");
        state = i->{
            if (i == SENTINEL) {
                startReadMessageLength();
            }
        };
    }

    private void startReadMessageLength() {
        log.debug("Reading length");
        bytesLeftInMessage = messageLength = 0;

        state = i->{
            messageLength = bytesLeftInMessage = i;
            if (messageLength > MAX_PAYLOAD_SIZE || messageLength < MIN_PAYLOAD_SIZE) {
                startLookingForMessageStart();
                state.accept(i); // Try to get back in sync.
                return;
            }
            crc.reset();
            crc.update(i);
            startReadPayload();
        };
    }

    private void startReadPayload() {
        log.debug("Reading payload");
        payloadInUse = 0;
        state = i->{
            bytesLeftInMessage--;
            if (bytesLeftInMessage > 1) {
                crc.update(i);
            }
            payload[payloadInUse++] = i;
            if (bytesLeftInMessage == 0) {
                parseFrame();
                startLookingForMessageStart();
            }
        };
    }

    private void parseFrame() {
        final byte endOfMessage = payload[payloadInUse - 1];
        if (endOfMessage != SENTINEL) {
            log.warn("Ignoring frame without correct ending sentinel: {}\n{}", String.format("%02x", endOfMessage), dumpPayload());
            return;
        }
        final byte crcFromFrame = payload[payloadInUse - 2];
        if (crcFromFrame != crc.getCRC8()) {
            log.warn("Ignoring frame with bad crc: {} should have been {}\n{}",
                    String.format("%02x", crcFromFrame),
                    String.format("%02x", crc.getCRC8()),
                    dumpPayload());
            return;
        }

        final byte channel = payload[0];
        // final byte bfaf = payload[1]; Not useful for anything that I can figure out
        final byte type = payload[2];

        frameConsumer.accept(new Frame(channel, type, ()->Arrays.copyOfRange(payload, 3, payloadInUse - 2)));
    }

    private String dumpPayload() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("Got %d bytes\n", messageLength));
        for (int i = 0; i < payloadInUse; i++) {
            sb.append(String.format(" %02x", payload[i]));
        }
        sb.append("\n");
        return sb.toString();
    }


    /**
     * Parses a single byte
     *
     * @param input The byte read from the spa controller
     */
    public void addByte(byte input) {
        state.accept(input);
    }
}
