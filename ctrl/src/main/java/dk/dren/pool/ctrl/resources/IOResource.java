package dk.dren.pool.ctrl.resources;

import dk.dren.pool.ctrl.api.IOState;
import dk.dren.pool.ctrl.io.IOController;
import dk.dren.pool.ctrl.io.Pump;
import dk.dren.pool.ctrl.io.PumpName;
import lombok.RequiredArgsConstructor;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/io")
@RequiredArgsConstructor
@Produces(MediaType.APPLICATION_JSON)
public class IOResource {
    private final IOController ioController;

    @GET
    public IOState get() throws IOException, InterruptedException {
        return ioController.getIOState();
    }

    @Path("pump/{name}")
    public PumpResource pump(@PathParam("name")String name) {

        final PumpName pumpName;
        try {
            pumpName = PumpName.valueOf(name.toUpperCase());
        } catch (Exception e) {
            throw new WebApplicationException("Pump named "+name+" doesn't exist", Response.Status.NOT_FOUND);
        }
        final Pump pump = ioController.getPumpByName().get(pumpName);
        if (pump == null) {
            throw new WebApplicationException("Pump named "+pumpName+" doesn't exist", Response.Status.NOT_FOUND);
        }
        return new PumpResource(pump);
    }
}
