package dk.dren.pool.ctrl.io;

import dk.dren.pool.ctrl.api.IOState;

import java.io.IOException;
import java.util.Map;

public interface IOController {
    IOState getIOState() throws IOException, InterruptedException;

    Map<PumpName, Pump> getPumpByName();
}
