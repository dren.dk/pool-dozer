package dk.dren.pool.ctrl.controller;

import dk.dren.pool.ctrl.api.IOState;
import dk.dren.pool.ctrl.config.ControllerConfig;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.util.Map;

@RequiredArgsConstructor
@Log
@Getter
public class ControllerInputs {
    /**
     * The number of degrees to add to a Celsius temperature to get Kelvin
     */
    public static final double CELCIUS_TO_KELVIN = 273.15;

    private final ControllerConfig ctrl;
    private ADCVoltage phVoltage;
    private ADCVoltage orpVoltage;
    private double voltPerpH;
    private double ph;
    private Float waterTemperature;
    private IOState ioState;
    private long lastAcidDose;
    private long lastOxidizerDose;
    private long lastJet;

    public void update(IOState ioState) {
        this.ioState = ioState;

        if (phVoltage == null) {
            phVoltage = new ADCVoltage(ctrl.getPh(), ioState.getPhADC());
        } else {
            phVoltage.update(ioState.getPhADC());
        }

        if (orpVoltage == null) {
            orpVoltage = new ADCVoltage(ctrl.getOrp(), ioState.getOrpADC());
        } else {
            orpVoltage.update(ioState.getOrpADC());
        }

        waterTemperature = ioState.getTemperatures().get(ctrl.getWaterTemperatureId());
        if (waterTemperature == null) {
            log.warning("Missing the configured water temperature sensor: "
                    +ctrl.getWaterTemperatureId()+" known sensors: "+dumpAllTemperatures(ioState.getTemperatures()));
            return;
        }

        voltPerpH = 0.000198*(waterTemperature +CELCIUS_TO_KELVIN);
        ph = 7-(phVoltage.getValue() / voltPerpH);

        if (ioState.getBalboaStatusUpdate().getPump1() != 0) {
            lastJet = System.currentTimeMillis();
        }
        if (ioState.isAcidPumpRunning()) {
            lastAcidDose = System.currentTimeMillis();
        }
        if (ioState.isOxidizerPumpRunning()) {
            lastOxidizerDose = System.currentTimeMillis();
        }
     }

    private String dumpAllTemperatures(Map<String, Float> temperatures) {
        StringBuffer sb = new StringBuffer();
        for (Map.Entry<String, Float> kv : temperatures.entrySet()) {
            sb.append(kv.getKey()).append("=").append(kv.getValue()).append("\n");
        }
        return sb.toString();
    }
}
