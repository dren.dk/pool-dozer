package dk.dren.pool.ctrl.hack;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Supplier;

@Slf4j
public class HackItUntilItWorks {

    public static <T> T run(Supplier<T> prod) {
        int patience = 10;
        while (true) {
            try {
                return prod.get();
            } catch (Exception e) {
                if (patience-- > 0) {
                    log.warn("Failed, retries left: {}", patience, e);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        throw e;
                    }
                } else {
                    log.warn("Failed even after retrying");
                    throw e;
                }
            }
        }
    }

}
