package dk.dren.pool.ctrl.balboa.message;

import dk.dren.pool.ctrl.balboa.Frame;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SetTemperature implements SendablePayload {
    public static final byte ID = 0x20;
    public static final byte WIFI_CHANNEL = (byte) 0x0a;

    @Getter
    private final double temperatureInC;

    public SetTemperature(Frame frame) {
        temperatureInC = frame.getPayload(0)/2.0;
    }

    @Override
    public Frame toFrame() {
        return new Frame(WIFI_CHANNEL, ID,
                (byte)Math.round(temperatureInC * 2));
    }
}
