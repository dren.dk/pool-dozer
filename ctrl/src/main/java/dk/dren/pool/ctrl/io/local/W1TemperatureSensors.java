package dk.dren.pool.ctrl.io.local;

import lombok.extern.java.Log;

import java.io.*;
import java.nio.file.Files;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

/**
 * Efficient bulk reading of the temperature of all connected one-wire temperature sensors
 *
 * With the bulk approach 7 sensors can be read in about 1150 ms, without it the time is 6200 ms.
 */
@Log
public class W1TemperatureSensors {
    private static final File root = new File("/sys/devices/w1_bus_master1");
    private static final File thermBulkRead = new File(root, "therm_bulk_read");
    private static final File deviceList = new File(root, "w1_master_slaves");

    public Map<String, Float> readAllTemperatures() throws IOException, InterruptedException {
        if (!thermBulkRead.canWrite()) {
            throw new RuntimeException("The trigger file "+thermBulkRead+" for 1-wire reading is not writeable, to fix it, try: sudo chmod a+rw /sys/devices/w1_bus_master1/therm_bulk_read");
        }
        try (final FileWriter fw = new FileWriter(thermBulkRead)) {
            fw.write("trigger\n");
        } catch (Exception e) {
            throw new IOException("Failed to write to "+thermBulkRead, e);
        }
        while (Files.readAllLines(thermBulkRead.toPath()).get(0).trim().equals("-1")) {
            Thread.sleep(100);
        }

        Map<String, Float> result = new TreeMap<>();
        for (String id : Files.readAllLines(deviceList.toPath())) {
            File tempFile = new File(root, id+"/temperature");
            if (tempFile.isFile() && tempFile.canRead()) {
                try (final BufferedReader br = new BufferedReader(new FileReader(tempFile))) {
                    final String line = br.readLine();
                    if (line != null) {
                        final float temperature = Integer.parseInt(line.trim()) / 1000f;
                        result.put(id, temperature);
                    } else {
                        log.warning("The file "+tempFile+" did not have the expected content");
                    }
                } catch (Exception e) {
                    log.log(Level.WARNING, "Ignoring exception while reading from "+id, e);
                }
            }
        }

        return result;
    }
}
