package dk.dren.pool.ctrl.io.local;

import com.pi4j.io.gpio.*;
import dk.dren.pool.ctrl.hack.HackItUntilItWorks;
import dk.dren.pool.ctrl.io.Pump;
import dk.dren.pool.ctrl.io.PumpName;
import lombok.Getter;
import lombok.extern.java.Log;

import java.util.logging.Level;

/**
 * Controls a pump that's attached to a raspberry pi GPIO pin
 */
@Log
public class LocalPump implements Pump {
    @Getter
    private final PumpName name;
    private final GpioPinDigitalOutput pin;
    private Thread stopper;

    public LocalPump(PumpName name, Pin gpioPin) {
        this.name = name;
        final GpioController gpio = GpioFactory.getInstance();
        pin = HackItUntilItWorks.run(()->gpio.provisionDigitalOutputPin(gpioPin));
        pin.setShutdownOptions(true, PinState.LOW);
    }

    public void run() {
        stopStopper();
        pin.high();
    }

    public void stop() {
        stopStopper();
        pin.low();
    }

    public boolean isRunning() {
        return pin.isHigh();
    }

    public void run(int ms) {
        stopStopper();
        stopper = new Thread(() -> {
            log.info("Going to run "+name+" pump on "+pin+" for "+ms+" ms");
            pin.high();
            try {
                Thread.sleep(ms);
            } catch (InterruptedException e) {
                // Meh.
            }
            pin.low();
            log.info("Stopped "+name+" pump on "+pin+" after "+ms+" ms");
        });
        stopper.setName("Running "+name+" pump on pin "+pin+" for "+ms+" ms");
        stopper.setDaemon(false);
        stopper.start();
    }

    private synchronized void stopStopper() {
        if (stopper != null) {
            Thread goner = stopper;
            stopper = null;
            try {
                goner.interrupt();
                goner.join();
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to stop the stopper for "+name, e);
            }
        }
    }
}
