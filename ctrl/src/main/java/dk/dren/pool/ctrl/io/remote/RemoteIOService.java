package dk.dren.pool.ctrl.io.remote;

import dk.dren.pool.ctrl.api.IOState;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * This is the raw REST interface expressed as an interface for retrofit to understand.
 * It directly matches the IOResource / PumpResource classes.
 */
public interface RemoteIOService {
    @GET("/io")
    Call<IOState> getState();

    @GET("/io/pump/{name}/running")
    Call<Boolean> isPumpRunning(@Path("name")String name);

    @POST("/io/pump/{name}/run")
    Call<Void> runPump(@Path("name")String name);

    @POST("/io/pump/{name}/run")
    Call<Void> runPump(@Path("name")String name, @Query("ms")Integer ms);

    @POST("/io/pump/{name}/stop")
    Call<Void> stopPump(@Path("name")String name);
}
