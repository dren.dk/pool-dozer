package dk.dren.pool.ctrl.controller;

import dk.dren.pool.ctrl.balboa.message.StatusUpdate;
import dk.dren.pool.ctrl.config.DozerConfig;
import dk.dren.pool.ctrl.controller.state.Idle;
import dk.dren.pool.ctrl.controller.state.WaterControllerState;
import dk.dren.pool.ctrl.io.IOController;
import io.dropwizard.lifecycle.Managed;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.java.Log;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@Getter
@Log
@RequiredArgsConstructor
public class WaterController implements Managed {

    private final DozerConfig configuration;
    private Thread controllerThread;
    private ControllerInputs inputs;
    @Setter
    private WaterControllerState state = null;

    private void poll() throws InterruptedException, IOException {
        final IOController io = configuration.getIo().getIoController();
        inputs.update(io.getIOState());

        // Immediately fall back to idle state, if the circulation pump isn't running.
        if (!getInputs().getIoState().getBalboaStatusUpdate().isCirculationPumpRunning()) {
            Idle.transition(this);
        }


        log.info(String.format("pH: %.2f\tORP: %.0f mv\tTemp: %.2f\tcirc: %b\tsample: %b\tstate: %s",
                inputs.getPh(), inputs.getOrpVoltage().getValue()*1000,
                inputs.getWaterTemperature(),
                inputs.getIoState().getBalboaStatusUpdate().isCirculationPumpRunning(),
                inputs.getIoState().isSamplePumpRunning(),
                state.toString()
                ));


        state.poll(this);

        reportToInfluxDb();
    }

    private void reportToInfluxDb() {
        final InfluxDB influx = configuration.getInflux().getInfluxDB();
        if (influx == null) {
            return;
        }

        final Point.Builder point = Point.measurement("spa")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .tag("location", configuration.getInflux().getLocation())
                .addField("controller-state", state.getClass().getSimpleName())
                .addField("water-temp", inputs.getWaterTemperature())
                .addField("pH-voltage", inputs.getPhVoltage().getValue())
                .addField("pH", inputs.getPh())
                .addField("ORP", inputs.getOrpVoltage().getValue())
                .addField("pumping-acid", inputs.getIoState().isAcidPumpRunning())
                .addField("pumping-oxidizer", inputs.getIoState().isOxidizerPumpRunning())
                .addField("pumping-sample", inputs.getIoState().isSamplePumpRunning());
        for (Map.Entry<String, Float> temp : inputs.getIoState().getTemperatures().entrySet()) {
            point.addField("temp-"+temp.getKey(), temp.getValue());
        }

        final StatusUpdate balboa = inputs.getIoState().getBalboaStatusUpdate();
        if (balboa != null) {
            point
                    .addField("balboa-set-temperature", balboa.getSetTemperatureCelsius())
                    .addField("balboa-circulation", balboa.isCirculationPumpRunning())
                    .addField("balboa-jet", balboa.getPump1())
                    .addField("balboa-light", balboa.getLightStatus1())
                    .addField("balboa-heat", balboa.isHeating())
            ;

            final double waterTemperatureCelsius = balboa.getWaterTemperatureCelsius();
            if (waterTemperatureCelsius > 0) {
                point.addField("balboa-temperature", waterTemperatureCelsius);
            }
        }

        influx.write(point.build());
    }

    private void controlLoop() throws InterruptedException {
        inputs = new ControllerInputs(configuration.getController());
        Idle.transition(this);

        while (true) {
            try {
                poll();
            } catch (InterruptedException e) {
                throw e;
            } catch (Exception e) {
                log.log(Level.WARNING, "Got exception while polling, ignoring ", e);
            }
            Thread.sleep(TimeUnit.SECONDS.toMillis(configuration.getController().getInterval()));
        }
    }

    @Override
    public void start() throws Exception {
        controllerThread = new Thread(() -> {
            try {
                controlLoop();
            } catch (InterruptedException e) {
                log.log(Level.INFO, "Got interrupted, quitting", e);
            }
        });
        controllerThread.setName("Controller");
        controllerThread.setDaemon(true);
        controllerThread.start();
    }

    @Override
    public void stop() throws Exception {
        if (controllerThread != null) {
            controllerThread.interrupt();
            controllerThread.join();
            controllerThread = null;
        }
    }
}
