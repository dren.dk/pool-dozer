package dk.dren.pool.ctrl.controller.state;

import dk.dren.pool.ctrl.controller.WaterController;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Pause implements WaterControllerState {
    private final long timeToWaitFor;
    private final WaterControllerState nextState;

    public static void transition(WaterController waterController, long timeToWait, WaterControllerState nextState) {
        final Pause pauseState = new Pause(System.currentTimeMillis()+timeToWait, nextState);
        waterController.setState(pauseState);
    }

    @Override
    public void poll(WaterController waterController) {
        if (System.currentTimeMillis() > timeToWaitFor) {
            waterController.setState(nextState);
        }
    }

    @Override
    public String toString() {
        final long secondsToGo = (timeToWaitFor-System.currentTimeMillis())/1000;
        return "Pause "+secondsToGo+" s";
    }
}
