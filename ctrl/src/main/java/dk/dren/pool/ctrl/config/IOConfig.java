package dk.dren.pool.ctrl.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pi4j.io.gpio.RaspiPin;
import dk.dren.pool.ctrl.io.*;
import dk.dren.pool.ctrl.io.local.LTC2420;
import dk.dren.pool.ctrl.io.local.LocalIOController;
import dk.dren.pool.ctrl.io.local.LocalPump;
import dk.dren.pool.ctrl.io.remote.RemoteIOController;
import lombok.*;

import java.io.File;
import java.net.URI;

@Data
@ToString(doNotUseGetters = true)
public class IOConfig {

    /**
     * If set, then IO will be farmed off to a remote instance at this URI
     */
    @JsonProperty
    URI remote = null;

    /**
     * The GPIO pin used for clocking data out of the LTC2420 ADCs
     */
    @JsonProperty
    int ltc2420ClockPin = 0;

    /**
     * The GPIO pin used for reading data from the LTC2420 ADCs
     */
    @JsonProperty
    int ltc2420DataPin = 3;

    /**
     * The GPIO pin used to select the LTC2420 ADC that reads pH
     */
    @JsonProperty
    int ltc2420ChipSelectPinPh = 1;

    /**
     * The GPIO pin used to select the LTC2420 ADC that reads ORP
     */
    @JsonProperty
    int ltc2420ChipSelectPinORP = 2;

    /**
     * The GPIO pin used to control the sample pump
     */
    @JsonProperty
    int samplePumpPin = 5;

    /**
     * The GPIO pin used to control the acid pump
     */
    @JsonProperty
    int acidPumpPin = 6;

    /**
     * The GPIO pin used to control the chlorine pump
     */
    @JsonProperty
    int oxidizerPumpPin = 7;

    /**
     * The serial port that reads the balboa RS485 bus
     */
    @JsonProperty
    File balboaRS485Port = new File("/dev/ttyUSB0");

    @Getter(lazy = true)
    @Setter(AccessLevel.NONE)
    private final LTC2420 ltc2420 = new LTC2420(
            RaspiPin.getPinByAddress(ltc2420ClockPin),
            RaspiPin.getPinByAddress(ltc2420DataPin),
            RaspiPin.getPinByAddress(ltc2420ChipSelectPinPh),
            RaspiPin.getPinByAddress(ltc2420ChipSelectPinORP)
            );

    @Getter(lazy = true)
    @Setter(AccessLevel.NONE)
    private final LocalPump samplePump = new LocalPump(PumpName.SAMPLE, RaspiPin.getPinByAddress(samplePumpPin));

    @Getter(lazy = true)
    @Setter(AccessLevel.NONE)
    private final LocalPump acidPump = new LocalPump(PumpName.ACID, RaspiPin.getPinByAddress(acidPumpPin));

    @Getter(lazy = true)
    @Setter(AccessLevel.NONE)
    private final LocalPump oxidizerPump = new LocalPump(PumpName.OXIDIZER, RaspiPin.getPinByAddress(oxidizerPumpPin));

    @Setter(AccessLevel.NONE)
    @Getter(lazy = true)
    private final IOController ioController = createIOController();

    private IOController createIOController() {
        if (remote != null) {
            return new RemoteIOController(remote);
        } else {
            return new LocalIOController(getLtc2420(), getSamplePump(), getAcidPump(), getOxidizerPump(), balboaRS485Port);
        }
    }
}
