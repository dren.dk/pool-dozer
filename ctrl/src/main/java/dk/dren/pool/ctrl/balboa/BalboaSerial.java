package dk.dren.pool.ctrl.balboa;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import dk.dren.pool.ctrl.balboa.message.SendablePayload;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

/**
 * Opens a serial port and attaches a deframer to it, to feed frames to the frameConsumer.
 */
@Slf4j
public class BalboaSerial {
    private final SerialPort commPort;
    private final Deframer deframer;

    public BalboaSerial(File serialPort, Consumer<Frame> frameConsumer) {
        commPort = SerialPort.getCommPort(serialPort.getAbsolutePath());
        commPort.setBaudRate(115200);
        commPort.setNumDataBits(8);
        commPort.setParity(SerialPort.NO_PARITY);
        commPort.setNumStopBits(1);
        //commPort.setRs485ModeParameters(true, true, 0, 0);
        if (!commPort.openPort()) {
            throw new RuntimeException("Could not open serial port: " + serialPort);
        }

        deframer = new Deframer(frameConsumer);

        commPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_RECEIVED | SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            @Override
            public void serialEvent(SerialPortEvent event) {
                final byte[] receivedData = event.getReceivedData();
                if (receivedData.length != 0) {
                    for (byte data : receivedData) {
                        deframer.addByte(data);
                    }
                }
            }
        });
    }

    public void send(SendablePayload paylaod) {
        try {
            commPort.getOutputStream().write(paylaod.toFrame().toBytes());
        } catch (IOException e) {
            log.warn("Failed to send "+paylaod.getClass().getName(), e);
        }
    }
}
