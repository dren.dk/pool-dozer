package dk.dren.pool.ctrl.resources;

import dk.dren.pool.ctrl.io.Pump;
import lombok.RequiredArgsConstructor;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@RequiredArgsConstructor
public class PumpResource {
    private final Pump pump;

    @POST
    @Path("run")
    public void run(@QueryParam("ms")Integer ms) {
        if (ms != null) {
            pump.run(ms);
        } else {
            pump.run();
        }
    }

    @POST
    @Path("stop")
    public void stop() {
        pump.stop();
    }

    @GET
    @Path("running")
    public boolean isRunning() {
        return pump.isRunning();
    }
}
